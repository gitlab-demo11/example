# For Java 8, try this
# FROM openjdk:8-jdk-alpine

FROM maven:3.6 as builder
WORKDIR /build

COPY src /build/src
COPY pom.xml /build

RUN mvn clean install -DskipTests

# For Java 11, try this
FROM adoptopenjdk/openjdk11:alpine-jre
WORKDIR /app
# Refer to Maven build -> finalName
ARG JAR_FILE=target/spring-boot-elastic-apm-0.0.1-SNAPSHOT.jar

# cp target/spring-boot-web.jar /opt/app/app.jar
COPY --from=builder /build/${JAR_FILE} app.jar

# java -jar /opt/app/app.jar
ENTRYPOINT ["java","-jar","app.jar"]
